<?php
class User
{
    private $name, $age, $email;

    public function __construct(string $name, int $age, string $email) {
        $this->name = $name;
        $this->age = $age;
        $this->email = $email;
    }

    private function setName(string $name) {
        $this->name = $name;
    }

    private function setAge(int $age) {
        $this->age = $age;
    }

    public function __call($method, $arguments) {
        if(method_exists($this, $method)) {                  //Проверяем наличие метода в классе по имени метода
            switch($method) {
                case 'setName':
                    $this->setName(implode($arguments));     //Выполняем условия для setName, преобразуя данные аргументов из массива в строку
                    break;
                case 'setAge':
                    $this->setAge((int)implode($arguments)); //Выполняем условия для setAge, преобразуя данные аргументов из массива в строку, а затем в целое число
                    break;
            }
        } else {
        echo "Trying to call unexisting method $method with arguments "
        . implode(', ', $arguments). "\n";
        }
    }

    public function getAll() {
        echo 'Имя пользователя: ' . $this->name;
        echo'<br/>';
        echo 'Возраст пользователя: ' . $this->age;
        echo'<br/>';
        echo 'Адрес электронной почты: ' . $this->email;
        echo'<br/>';
    }
}
?>