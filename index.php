<?php
include_once(dirname(__FILE__) . '/user.php');

$user = new User('Johnny', 14, 'johnny@mail.com');
$user->getAll();
var_dump($user);
echo '<br/>';

$user->getEmail();                   //Вызываем несуществующий геттер
echo '<br/>';
$user->setEmail('blabla@mail.com');  //Вызываем несуществующий сеттер 
echo '<br/>';

$user->setName('David');
$user->setAge(57);
$user->getAll();
var_dump($user);
?>